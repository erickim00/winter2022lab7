

public class Board {

	private Die die1;
	private Die die2;
	private int[][] closedTiles;
	final int fini = 3;
	
	public Board() {
		this.die1 = new Die();
		this.die2 = new Die();
		this.closedTiles = new int[6][6];
		for(int i=0,o=0; i < this.closedTiles.length;i++,o++) {
			this.closedTiles[i][o] = 0;
		}
		}
	public String toString() {
		String s = " ";
		for (int i =0 ; i < closedTiles.length ;i++) {
			for(int o=0; o<this.closedTiles[i].length; o++) {
				System.out.println("closedTiles["+(i+1)+"]["+(o+1)+"]: "+ closedTiles[i][o]);
			if(this.closedTiles[i][o] == fini) {
				System.out.println("Game over!");
				break;
				}
			}
		}
		return s;
	}
	public boolean playATurn() {
		boolean check = false;
		die1.roll();
		die2.roll();
		System.out.println("Die 1: "+die1.getPips());
		System.out.println("Die 2: "+die2.getPips());
		closedTiles[die1.getPips()-1][die2.getPips()-1]=1+closedTiles[die1.getPips()-1][die2.getPips()-1];
		
		if(closedTiles[die1.getPips()-1][die2.getPips()-1]==fini) {
			check = true;
			
		}
		else {
			check = false;
		}
			
		
		return check;
	}
	
	
	

	
}
